function invert(obj)
{
    const invert_pair = {};
    for (let key in obj) 
    {
        invert_pair[obj[key]]=key;
    }
    return invert_pair;
}

module.exports=invert;