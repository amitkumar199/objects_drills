function mapObject(obj, cb) {
   const result={};
   for(let key in obj)
   {
    if(typeof obj[key] !== 'number')
    {
        result[key]=key;
    }
    else
    {
        result[key] =cb(obj[key]);
    }
    
   }
   return result;
  }
module.exports=mapObject;